
export const fixedAndDrop = (data: number | string, countSymbol?: number): string => {
    if (data === undefined || data == null) {
        return '--';
    } else {
        countSymbol = countSymbol === undefined ? 1 : countSymbol;
        let fixedData = parseFloat(data.toString()).toFixed(countSymbol);
        if (countSymbol === 0) {
            return fixedData;
        }
        while (true) {
            if (fixedData.slice(-1)[0] === '0') {
                fixedData = fixedData.slice(0, -1);
            } else if (fixedData.slice(-1)[0] === '.') {
                fixedData = fixedData.slice(0, -1);
                break;
            } else {
                break;
            }
        }
        return fixedData;
    }
};


