import axios, {AxiosError, AxiosResponse} from 'axios';
import {PlacingVariantsLocationsJSON, ScenariosJSON} from "../stores/RootStore/PlacingStore/BlockLocationsStore/types";
import {createFormData} from "./helpers";

export const blockLocationsApi = {
    getScenarios() {
        return axios.get('/blocks_locations/scenarios')
            .then((res: AxiosResponse): ScenariosJSON=> {
                return res.data;
            }).catch(function (err) {
                return null;
            });
    },
    getPlacingVariantsLocations() {
        let sendData = createFormData({
            id: 1
        });
        return axios.post('/blocks_locations/placing-variants', sendData)
            .then((res: AxiosResponse): PlacingVariantsLocationsJSON => {
                return res.data;
            }).catch(function (err) {
                return null;
            });
    }
}