export const  createFormData = (data: {[key: string]: any}) : FormData => {
    let requestFormData = new FormData;
    Object.entries(data).forEach(([key, value]: [string, any]) => {requestFormData.append(key,value)});
    return requestFormData;
};