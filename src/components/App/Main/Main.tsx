import {inject, observer} from "mobx-react";
import React from "react";
import { RootStore } from "../../../stores/RootStore/RootStore";
import styled from "styled-components/macro";
import { Router, Route, Redirect } from "react-router-dom";
import Placing from "./Placing/Placing";
import PreloaderSphere from "../../BasicComponents/Preloaders/PreloaderSphere";
import PreloderContainerBlur from "../../BasicComponents/Preloaders/PreloderContainerBlur/PreloderContainerBlur";

const MainContainer = styled.div`
  flex: 1 1;
  min-height: 0rem;
  background-color: white;
  overflow: auto;
`

interface MainProps {
    rootStore?: RootStore
}

export const Main: React.FC<MainProps> = inject('rootStore')(observer((props: MainProps) => {
    const {navigationStore: {history}, preloaderStore : {isLoading} } = props.rootStore!;

    return (
        <MainContainer>
            <Router {...{history}}>
                    <PreloderContainerBlur {...{isLoading, preloader: <PreloaderSphere/>}}>
                        <Route exact path="/">
                            <Redirect to="/placing"/>
                        </Route>
                        <Route path="/placing" component={Placing}/>
                    </PreloderContainerBlur>
            </Router>
        </MainContainer>
    )
}));

export default Main
