import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import { RootStore } from "../../../../../stores/RootStore/RootStore";

const DepartmentsBuildingsContainer = styled.div`
  width: 100%;
  height: 100%;
`

interface DepartmentsBuildingsProps {
    rootStore?: RootStore
}

export const DepartmentsBuildings: React.FC<DepartmentsBuildingsProps> = inject('rootStore')(observer((props: DepartmentsBuildingsProps) => {

    return (
        <DepartmentsBuildingsContainer>
            Размещение по этажам
        </DepartmentsBuildingsContainer>
    )
}));

export default DepartmentsBuildings
