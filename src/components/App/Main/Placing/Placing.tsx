import {inject, observer} from "mobx-react";
import React from "react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../stores/RootStore/RootStore";
import {Redirect, Route, RouteComponentProps} from "react-router-dom";
import BlockLocations from "./BlockLocations/BlockLocations";
import DepartmentsBuildings from "./DepartmentsBuildings/DepartmentsBuildings";

const PlacingContainer = styled.div`
  width: 100%;
  height: 100%;
`

export const PlacingAdaptiveContainer = styled.div`
    width: 100%;
    margin: 0 auto;
    padding: 10rem;
    box-sizing: border-box;
    height: 100%;

    @media (min-width: 1440px){
          max-width: 90%;
    }

    @media (min-width: 1930px){
          max-width: 80%;
    }
`;

interface PlacingProps extends RouteComponentProps{
    rootStore?: RootStore
}

export const Placing: React.FC<PlacingProps> = inject('rootStore')(observer((props: PlacingProps) => {
    const {path} = props.match;

    return (
        <PlacingContainer>
            <Route exact path={`${path}/`}>
                <Redirect to={`${path}/block-locations`}/>
            </Route>
            <Route path={`${path}/block-locations`} component={BlockLocations}/>
            <Route path={`${path}/departments-buildings`} component={DepartmentsBuildings}/>
        </PlacingContainer>
    )
}));

export default Placing
