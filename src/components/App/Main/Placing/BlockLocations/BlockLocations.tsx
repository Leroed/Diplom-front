import React, {useEffect} from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../stores/RootStore/RootStore";
import PlacingVariantsLocations from "./PlacingVariantsLocations/PlacingVariantsLocations";
import ScenarioLocations from "./ScenarioLocations/ScenarioLocations";
import { PlacingAdaptiveContainer } from "../Placing";

const BlockLocationsContainer = styled(props => (<PlacingAdaptiveContainer {...props}/>))`
  width: 100%;
  height: 100%;
`

export const BlockLocationsGroup = styled.div`
  border-radius: 5rem;
  border: 1rem solid grey;
  box-shadow: 6px 0 8px -2px rgba(0,0,0,0.61), -6px 0 8px -2px rgba(0,0,0,0.61);
  min-height: 200rem;
  margin-top: 10rem;
  box-sizing: border-box;
    padding: 10rem;
  &:last-child{
  margin-bottom: 10rem;
  }
`

interface BlockLocationsProps {
    rootStore?: RootStore
}

export const BlockLocations: React.FC<BlockLocationsProps> = inject('rootStore')(observer((props: BlockLocationsProps) => {
    const {isLoadingStatus} = props.rootStore!.preloaderStore;
    const {placingVariantsLocationsTable, currentScenarioTable} = props.rootStore!.placingStore.blockLocationsStore;

    useEffect(() => {
        if(placingVariantsLocationsTable && currentScenarioTable){
            isLoadingStatus(false);
        }
    }, [placingVariantsLocationsTable, currentScenarioTable]);

    return (
        <BlockLocationsContainer>
            <ScenarioLocations/>
            <PlacingVariantsLocations/>
        </BlockLocationsContainer>
    )
}));

export default BlockLocations
