import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../../stores/RootStore/RootStore";

const PlacingVariantsLocationsCellContainer = styled.div`
  display: flex;
  align-items: center;
  min-height: 100%;
  min-width: 10rem;
  box-sizing: border-box;
  flex: 1 1;
`

interface PlacingVariantsLocationsCellProps {
    children: React.ReactNode,
    rootStore?: RootStore
}

export const PlacingVariantsLocationsCell: React.FC<PlacingVariantsLocationsCellProps> = inject('rootStore')(observer((props: PlacingVariantsLocationsCellProps) => {
    const {children} = props;
    return (
        <PlacingVariantsLocationsCellContainer>
            {children}
        </PlacingVariantsLocationsCellContainer>
    )
}));

export default PlacingVariantsLocationsCell
