import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../../stores/RootStore/RootStore";
import {PlacingVariantsLocationsTableItem} from "../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";
import PlacingVariantsLocationsCell from "../PlacingVariantsLocationsCell/PlacingVariantsLocationsCell";
import {PlacingVariantsLocationsHeaderLabel} from "../PlacingVariantsLocationsHeaderLabel/PlacingVariantsLocationsHeaderLabel";
import PlacingVariantsLocationsCellBlock from "../PlacingVariantsLocationsCellBlock/PlacingVariantsLocationsCellBlock";
import PlacingVariantsLocationsCellLabel from "../PlacingVariantsLocationsCellLabel/PlacingVariantsLocationsCellLabel";

const PlacingVariantsLocationsRowContainer = styled.div`
  display: flex;
  flex-direction: row;
  min-height: 35rem;
  box-sizing: border-box;
  flex-wrap: nowrap;
`

interface PlacingVariantsLocationsRowProps {
    values: PlacingVariantsLocationsTableItem[],
    rootStore?: RootStore
}

export const PlacingVariantsLocationsRow: React.FC<PlacingVariantsLocationsRowProps> = inject('rootStore')(observer((props: PlacingVariantsLocationsRowProps) => {
    const {values} = props;

    return (
        <PlacingVariantsLocationsRowContainer>
            {
                values.map((item: PlacingVariantsLocationsTableItem) => {
                    return <PlacingVariantsLocationsCell key={item.id}>
                        {
                            item.type === 'block' ?
                                <PlacingVariantsLocationsCellBlock {...{data: item}}/>
                                :
                                <PlacingVariantsLocationsCellLabel {...{data: item}}/>
                        }
                    </PlacingVariantsLocationsCell>
                })
            }
        </PlacingVariantsLocationsRowContainer>
    )
}));

export default PlacingVariantsLocationsRow
