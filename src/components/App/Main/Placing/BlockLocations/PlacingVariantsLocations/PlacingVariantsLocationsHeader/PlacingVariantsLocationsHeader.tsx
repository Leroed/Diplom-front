import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import { RootStore } from "../../../../../../../stores/RootStore/RootStore";
import {
    PlacingVariantsLocationsTableItem,
    ScenarioTableHeaderItemType
} from "../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";
import { PlacingVariantsLocationsHeaderLabel } from "../PlacingVariantsLocationsHeaderLabel/PlacingVariantsLocationsHeaderLabel";
import PlacingVariantsLocationsCell from "../PlacingVariantsLocationsCell/PlacingVariantsLocationsCell";

const PlacingVariantsLocationsHeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  min-height: 35rem;
  box-sizing: border-box;
  flex-wrap: nowrap;
`

interface PlacingVariantsLocationsHeaderProps {
    headers: PlacingVariantsLocationsTableItem[],
    rootStore?: RootStore
}

export const PlacingVariantsLocationsHeader: React.FC<PlacingVariantsLocationsHeaderProps> = inject('rootStore')(observer((props: PlacingVariantsLocationsHeaderProps) => {
    const {headers} = props;

    return (
        <PlacingVariantsLocationsHeaderContainer>
            {
                headers.map((item: PlacingVariantsLocationsTableItem) => {
                    return <PlacingVariantsLocationsCell key={item.id}>
                        <PlacingVariantsLocationsHeaderLabel {...{title:item.value}}>
                            {item.value}
                        </PlacingVariantsLocationsHeaderLabel>
                    </PlacingVariantsLocationsCell>
                })
            }
        </PlacingVariantsLocationsHeaderContainer>
    )
}));

export default PlacingVariantsLocationsHeader
