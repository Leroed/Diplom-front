import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../../stores/RootStore/RootStore";
import {
    PlacingVariantsLocationsTableItem,
    PlacingVariantsLocationsTableItemTypes
} from "../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";

const PlacingVariantsLocationsCellLabelContainer = styled.div`
  width: 100%;
  display: flex;
`

const PlacingVariantsLocationsCellLabelValue = styled.div<{type: PlacingVariantsLocationsTableItemTypes}>`
  font-size: 12rem;
  ${props => {
      if(props.type === "header"){
          return 'font-weight: bold'
      }
}};
  word-break: break-word;
  -webkit-line-clamp: 1;
  max-height: 15rem;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  margin: auto;
`


interface PlacingVariantsLocationsCellLabelProps {
    data: PlacingVariantsLocationsTableItem,
    rootStore?: RootStore
}

export const PlacingVariantsLocationsCellLabel: React.FC<PlacingVariantsLocationsCellLabelProps> = inject('rootStore')(observer((props: PlacingVariantsLocationsCellLabelProps) => {
    const {data: {value, type}} = props;

    return (
        <PlacingVariantsLocationsCellLabelContainer>
            <PlacingVariantsLocationsCellLabelValue {...{type}}>
                {value}
            </PlacingVariantsLocationsCellLabelValue>
        </PlacingVariantsLocationsCellLabelContainer>
    )
}));

export default PlacingVariantsLocationsCellLabel
