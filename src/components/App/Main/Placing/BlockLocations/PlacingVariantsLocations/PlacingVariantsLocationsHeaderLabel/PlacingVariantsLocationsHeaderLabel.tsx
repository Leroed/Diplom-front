import React, {ReactNode} from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../../stores/RootStore/RootStore";

const PlacingVariantsLocationsHeaderLabelContainer = styled.div`
  width: 100%;
  display: flex;
`

const PlacingVariantsLocationsHeaderLabelValue = styled.div`
  wfont-size: 12rem;
  font-weight: bold;
  word-break: break-word;
  -webkit-line-clamp: 1;
  max-height: 15rem;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  margin: auto;
`


interface PlacingVariantsLocationsHeaderLabelProps {
    title: string
    children?: ReactNode,
    rootStore?: RootStore
}

export const PlacingVariantsLocationsHeaderLabel: React.FC<PlacingVariantsLocationsHeaderLabelProps> = inject('rootStore')(observer((props: PlacingVariantsLocationsHeaderLabelProps) => {
    const {children, title} = props;

    return (
        <PlacingVariantsLocationsHeaderLabelContainer title={title}>
            <PlacingVariantsLocationsHeaderLabelValue>
                {children}
            </PlacingVariantsLocationsHeaderLabelValue>
        </PlacingVariantsLocationsHeaderLabelContainer>
    )
}));

export default PlacingVariantsLocationsHeaderLabel
