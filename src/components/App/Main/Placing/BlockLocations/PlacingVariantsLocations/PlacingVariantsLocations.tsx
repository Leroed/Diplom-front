import React, {useEffect} from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../stores/RootStore/RootStore";
import {BlockLocationsGroup} from "../BlockLocations";
import {
    LocationItemType,
    PlacingVariantsLocationsTableType,
    ScenarioTableType
} from "../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";
import PlacingVariantsLocationsHeader from "./PlacingVariantsLocationsHeader/PlacingVariantsLocationsHeader";
import PlacingVariantsLocationsRow from "./PlacingVariantsLocationsRow/PlacingVariantsLocationsRow";
import {dictLocations} from "../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/helpers";

const PlacingVariantsLocationsContainer = styled(props => <BlockLocationsGroup {...props} />)`
  
`

const PlacingVariantsLocationsTitleRow = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10rem 0rem;
`

const PlacingVariantsLocationsTitle = styled.div`
  font-size: 16rem;
  margin-left: 20rem;
`

const PlacingVariantsLocationsLegends = styled.div`
  display: flex;
`

const PlacingVariantsLocationsLegend = styled.div`
  display: flex;
  align-items: center;
`

const PlacingVariantsLocationsLegendBlock = styled.div`
  width: 40rem;
 height: 40rem;
  padding: 5rem;
  box-sizing: border-box;
  display: flex;
`

const PlacingVariantsLocationsLegendBlockRectangle = styled.div<{color: string}>`
  border-radius: 3rem;
  width: 100%;
  min-height: 100%;
  background-color: ${props => (props.color)};
`

const PlacingVariantsLocationsLegendText = styled.div`
  font-size: 16rem;
`

interface PlacingVariantsLocationsProps {
    rootStore?: RootStore
}

export const PlacingVariantsLocations: React.FC<PlacingVariantsLocationsProps> = inject('rootStore')(observer((props: PlacingVariantsLocationsProps) => {
    const {getPlacingVariantsLocations, placingVariantsLocationsTable, locations} = props.rootStore!.placingStore.blockLocationsStore;
    const placingVariantsLocationsTableData =  placingVariantsLocationsTable || ({headers: [], rows: []} as PlacingVariantsLocationsTableType);
    const {headers, rows} = placingVariantsLocationsTableData;

    useEffect(() => {
        getPlacingVariantsLocations()
    }, []);

    return (
        <PlacingVariantsLocationsContainer>
            <PlacingVariantsLocationsTitleRow>
                <PlacingVariantsLocationsTitle>Варианты размещения</PlacingVariantsLocationsTitle>
                <PlacingVariantsLocationsLegends>
                {locations.map((item:LocationItemType) => (
                    <PlacingVariantsLocationsLegend>
                        <PlacingVariantsLocationsLegendBlock>
                            <PlacingVariantsLocationsLegendBlockRectangle {...{color: item.color}}/>
                        </PlacingVariantsLocationsLegendBlock>
                        <PlacingVariantsLocationsLegendText>
                            {dictLocations[item.name]}
                        </PlacingVariantsLocationsLegendText>
                    </PlacingVariantsLocationsLegend>
                ))}
                </PlacingVariantsLocationsLegends>
            </PlacingVariantsLocationsTitleRow>
            <PlacingVariantsLocationsHeader {...{headers}} />
            {rows.map(({values, id}) => (
                    <PlacingVariantsLocationsRow key={id} {...{values}}/>
                ))
            }
        </PlacingVariantsLocationsContainer>
    )
}));

export default PlacingVariantsLocations
