import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../../stores/RootStore/RootStore";
import {PlacingVariantsLocationsTableItem} from "../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";

const PlacingVariantsLocationsCellBlockContainer = styled.div`
  width: 100%;
  min-height: 100%;
  padding: 5rem;
  box-sizing: border-box;
  display: flex;
`

const PlacingVariantsLocationsCellBlockRectangle = styled.div<{color: string}>`
  border-radius: 3rem;
  width: 100%;
  min-height: 100%;
  background-color: ${props => (props.color)};
`

interface PlacingVariantsLocationsCellBlockProps {
    data: PlacingVariantsLocationsTableItem,
    rootStore?: RootStore
}

export const PlacingVariantsLocationsCellBlock: React.FC<PlacingVariantsLocationsCellBlockProps> = inject('rootStore')(observer((props: PlacingVariantsLocationsCellBlockProps) => {
    const {data: {value}} = props;

    return (
        <PlacingVariantsLocationsCellBlockContainer>
            <PlacingVariantsLocationsCellBlockRectangle {...{color: value}}>
                &nbsp;
            </PlacingVariantsLocationsCellBlockRectangle>
        </PlacingVariantsLocationsCellBlockContainer>
    )
}));

export default PlacingVariantsLocationsCellBlock
