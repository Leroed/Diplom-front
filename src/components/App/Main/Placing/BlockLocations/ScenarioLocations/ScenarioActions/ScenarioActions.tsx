import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../../stores/RootStore/RootStore";
import { SelectBox } from "../../../../../../BasicComponents/BasicInputBox/SelectBox/SelectBox";

const ScenarioActionsContainer = styled.div`
   display: flex;
   box-sizing: border-box;
   padding: 20rem 0rem;
    box-sizing: border-box;
    border-top: 1rem solid grey;
    align-items: center;
    justify-content: space-between
`

const ScenarioActionFirstTen = styled.div`
    display: flex;
    flex-direction: column;
`

const ScenarioActionFirstTenTitle = styled.span`
  font-size: 16rem;
`

const ScenarioActionSelect = styled.div`
  display: flex;
  
`

const ScenarioActionSelectTitle = styled.div`
  margin-right: 10rem;
  font-size: 16rem;
`

const ScenarioActionSelectUpdateCalcButton = styled.button`
  
  height: 35rem;
  background-color: white;
  border: 1rem solid rgb(50,50,50);
  color: black;
  width: 150rem;
  transition: all 0.5s;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor:pointer;
  outline: unset;
  border-radius: 3rem;
  font-size: 16rem;
  
  &:hover{
    background-color: rgb(50,50,50);
    color: white;
  }
  
`

const ScenarioActionFirstTenSwitch = styled.label`
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
`

const ScenarioActionFirstTenSpan = styled.span`
  
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
    
    &:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
}

  border-radius: 34px;
  
  &:before {
    border-radius: 50%;
}
`

const ScenarioActionFirstTenInput = styled.input`
    opacity: 0;
    width: 0;
    height: 0;
    &:checked + ${ScenarioActionFirstTenSpan} {
    background-color: #1c7a27;
}
    &:focus + ${ScenarioActionFirstTenSpan} {
        box-shadow: 0 0 1px #1c7a27;
}
    &:checked + ${ScenarioActionFirstTenSpan}:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
}
`

export const ScenarioActionSelectBox = styled.div`
   width: 160rem;
   min-height: 20rem;
   display: flex;
   align-items: center;
   pointer-events: auto;
   
   
   .simple-select {
    /* height: 100%; */
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    position: relative;
    background-color: rgb(43, 43, 43);
  }
  
  .simple-select_header, .multi-select_header {
    width: 100%;
    min-height: 20rem;
    box-sizing: border-box;
    display: grid;
    grid-template-areas: "simple-select_header-lbl select-unfold-btn";
    grid-template-columns: 1fr 15rem;
    grid-template-rows: 100%;
    z-index: 0;
    border: 1rem solid rgb(95, 95, 95);
}

.checked-select_lbl {
    font-size: 13rem;
    color: white;
}

.select-unfold-btn {
    grid-area: select-unfold-btn;
    width: 20rem;
    height: 20rem;
}

.select-unfold-btn g {
   fill: white;
}

.simple-select_options, .advanced-select_options {
    width: 100%;
    position: absolute;
    top: 100%;
    background-color: rgb(43, 43, 43);
    //background: #2d2f3e;
    z-index: 1;
    max-height: 200rem;
    overflow: auto;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
    border-bottom-left-radius: 7px;
    border-bottom-right-radius: 7px;
}

.select-item {
    display: grid;
    grid-template-areas: "select-item_value select-item_check-btn";
    grid-template-columns: 1fr 17rem;
    cursor: pointer;
    width: 100%;
    min-height: 30rem;
    height: auto;
    border-left: 1rem solid rgb(95, 95, 95);
    border-right: 1rem solid rgb(95, 95, 95);
    border-bottom: 1rem solid rgb(95, 95, 95);
    box-sizing: border-box;
    
    }
    
    .select-item:last-child{
    border-bottom-left-radius: 7px;
    border-bottom-right-radius: 7px;
    }
    
    .select-item_value {
    grid-area: select-item_value;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    font-size: 12rem;
    color: white;
}
`


interface ScenarioActionsProps {
    rootStore?: RootStore
}

export const ScenarioActions: React.FC<ScenarioActionsProps> = inject('rootStore')(observer((props: ScenarioActionsProps) => {
    const {selectScenario, onSelectScenario, scenarioOptions, getPlacingVariantsLocations} = props.rootStore!.placingStore.blockLocationsStore;

    return (
        <ScenarioActionsContainer>
            <ScenarioActionFirstTen>
                <ScenarioActionFirstTenTitle>Лучшие 10</ScenarioActionFirstTenTitle>
                <ScenarioActionFirstTenSwitch>
                    <ScenarioActionFirstTenInput type={'checkbox'}></ScenarioActionFirstTenInput>
                    <ScenarioActionFirstTenSpan></ScenarioActionFirstTenSpan>
                </ScenarioActionFirstTenSwitch>
            </ScenarioActionFirstTen>
            <ScenarioActionSelect>
                <ScenarioActionSelectTitle>Сценарий:</ScenarioActionSelectTitle>
                <ScenarioActionSelectBox>
                    {   selectScenario &&
                        <SelectBox {...{
                            select: selectScenario,
                            onSelectItemClick: onSelectScenario,
                            listOptions: scenarioOptions,
                            isAdvanced: false
                        }}/>
                    }
                </ScenarioActionSelectBox>
            </ScenarioActionSelect>
            <ScenarioActionSelectUpdateCalcButton onClick={() => {getPlacingVariantsLocations()}}>Обновить</ScenarioActionSelectUpdateCalcButton>
        </ScenarioActionsContainer>
    )
}));

export default ScenarioActions
