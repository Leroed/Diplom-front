import React, {useEffect} from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../stores/RootStore/RootStore";
import { BlockLocationsGroup } from "../BlockLocations";
import ScenarioSettings from "./ScenarioSettings/ScenarioSettings";
import ScenarioActions from "./ScenarioActions/ScenarioActions";

const ScenarioLocationsTitle = styled.div`
  margin-left: 20rem;
  font-size: 16rem;
`

const ScenarioLocationsContainer = styled(props => <BlockLocationsGroup {...props} />)`
  box-sizing: border-box;
  padding: 10rem;
`

interface ScenarioLocationsProps {
    rootStore?: RootStore
}

export const ScenarioLocations: React.FC<ScenarioLocationsProps> = inject('rootStore')(observer((props: ScenarioLocationsProps) => {

    const {restoreData,getScenarios, currentScenarioTable} = props.rootStore!.placingStore.blockLocationsStore;
    const {isLoadingStatus} = props.rootStore!.preloaderStore;

    useEffect(() => {
        getScenarios();
        return () => {
            restoreData()
        };
    }, []);

    // useEffect(() => {
    //     if(currentScenarioTable){
    //         isLoadingStatus(false);
    //     }
    // }, [currentScenarioTable]);
    
    return (
        <ScenarioLocationsContainer>
            <ScenarioLocationsTitle>
                Настройка сценария моделирования
            </ScenarioLocationsTitle>
            <ScenarioSettings/>
            <ScenarioActions/>
        </ScenarioLocationsContainer>
    )
}));

export default ScenarioLocations
