import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../../../stores/RootStore/RootStore";
import { ScenarioTableCellTypeVariant } from "../../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";

const ScenarioTableCellContainer = styled.div<{type: ScenarioTableCellTypeVariant | undefined}>`
  display: flex;
  align-items: center;
  min-height: 100%;
  min-width: 10rem;
  box-sizing: border-box;
  flex: 1 1;
  ${props => {
      if(props.type && props.type === 'header'){
          return `min-width: 100rem;`
      }
}}
`

interface ScenarioTableCellProps {
    children: React.ReactNode,
    type?: ScenarioTableCellTypeVariant,
    rootStore?: RootStore
}

export const ScenarioTableCell: React.FC<ScenarioTableCellProps> = inject('rootStore')(observer((props: ScenarioTableCellProps) => {
    const {children, type} = props;

    return (
        <ScenarioTableCellContainer {...{type}}>
            {children}
        </ScenarioTableCellContainer>
    )
}));

export default ScenarioTableCell
