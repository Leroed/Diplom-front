import {ScenarioTableCellInput} from "./ScenarioTableCellInput/ScenarioTableCellInput";
import ScenarioTableCellHeader from "./ScenarioTableCellHeader/ScenarioTableCellHeader";
import ScenarioTableCellBlock from "./ScenarioTableCellBlock/ScenarioTableCellBlock";
import ScenarioTableCellLabel from "./ScenarioTableCellLabel/ScenarioTableCellLabel";
import React from "react";
import {
    ScenarioTableCellBlockType,
    ScenarioTableCellHeaderType,
    ScenarioTableCellInputType,
    ScenarioTableCellLabelType, ScenarioTableRowItemValue
} from "../../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";
import {isOfType} from "../../../../../../../../helpers/typesMethods";

const ScenarioTableCellDict: {
    input: React.FC<{ data: ScenarioTableCellInputType }>,
    label: React.FC<{ data: ScenarioTableCellLabelType }>,
    header: React.FC<{ data: ScenarioTableCellHeaderType }>,
    block: React.FC<{ data: ScenarioTableCellBlockType, selectScenariosItemBlock: (id: string) => void}>
} = {
    "input": ScenarioTableCellInput,
    "label": ScenarioTableCellLabel,
    "header": ScenarioTableCellHeader,
    "block": ScenarioTableCellBlock
};

export const getScenarioTableCellReactNode = (
    {data, selectScenariosItemBlock}:
        { data: ScenarioTableRowItemValue, selectScenariosItemBlock: (id: string) => void }
): React.ReactNode => {
    if (data.type === "input") {
        return <ScenarioTableCellInput data={data as ScenarioTableCellInputType}/>
    } else if (data.type === "label") {
        return <ScenarioTableCellLabel data={data as ScenarioTableCellLabelType}/>
    } else if (data.type === "header") {
        return <ScenarioTableCellHeader data={data as ScenarioTableCellHeaderType}/>
    } else if (data.type === "block") {
        return <ScenarioTableCellBlock data={data as ScenarioTableCellBlockType} selectScenariosItemBlock = {selectScenariosItemBlock}/>
    }
    return null
};