import React, {Component} from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {
    ScenarioTableRowItemValue,
} from "../../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";
import {RootStore} from "../../../../../../../../stores/RootStore/RootStore";
import {getScenarioTableCellReactNode} from "./helpers";

const ScenarioTableCellOptionalContainer = styled.div`
   width: 100%;
   min-height: 100%;
   display: flex;
`

interface ScenarioTableCellOptionalProps {
    data: ScenarioTableRowItemValue,
    selectScenariosItemBlock: (id: string) => void,
    rootStore?: RootStore
}

export const ScenarioTableCellOptional: React.FC<ScenarioTableCellOptionalProps> = inject('rootStore')(observer((props: ScenarioTableCellOptionalProps) => {
    const {data, selectScenariosItemBlock} = props;

    return (
        <ScenarioTableCellOptionalContainer>
            {getScenarioTableCellReactNode({data,selectScenariosItemBlock})}
        </ScenarioTableCellOptionalContainer>
    )
}));

export default ScenarioTableCellOptional
