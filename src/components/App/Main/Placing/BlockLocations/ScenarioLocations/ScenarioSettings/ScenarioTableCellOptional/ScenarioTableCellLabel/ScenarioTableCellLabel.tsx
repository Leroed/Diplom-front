import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../../../../stores/RootStore/RootStore";
import {
    ScenarioTableCellHeaderType,
    ScenarioTableRowItemValue,
    ScenarioTableCellLabelType
} from "../../../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";

const ScenarioTableCellLabelContainer = styled.div`
  width: 100%;
  display: flex;
`

const ScenarioTableCellLabelValue = styled.span`
  font-size: 12rem;
  word-break: break-word;
  -webkit-line-clamp: 1;
  max-height: 15rem;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  margin: auto;

`

interface ScenarioTableCellLabelProps {
    data: ScenarioTableCellLabelType,
    rootStore?: RootStore
}

export const ScenarioTableCellLabel: React.FC<ScenarioTableCellLabelProps> = inject('rootStore')(observer((props: ScenarioTableCellLabelProps) => {
    const {data: {value}} = props;
    return (
        <ScenarioTableCellLabelContainer>
            <ScenarioTableCellLabelValue>
                {value}
            </ScenarioTableCellLabelValue>
        </ScenarioTableCellLabelContainer>
    )
}));

export default ScenarioTableCellLabel
