import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import { RootStore } from "../../../../../../../../../stores/RootStore/RootStore";
import {
    ScenarioTableCellInputType
} from "../../../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";

const ScenarioTableCellInputContainer = styled.div`
  display: flex;
  width: 100%;
`

const ScenarioTableCellInputCapacity= styled.input`
 width: 100%;
`

interface ScenarioTableCellInputProps {
    data: ScenarioTableCellInputType,
    rootStore?: RootStore
}

export const ScenarioTableCellInput: React.FC<ScenarioTableCellInputProps> = inject('rootStore')(observer((props: ScenarioTableCellInputProps) => {
    const {data} = props;
    return (
        <ScenarioTableCellInputContainer>
            {data.id.split("-")[0] !== '999' &&
            <ScenarioTableCellInputCapacity readOnly={true} type = 'number' min={0} max={100} step={1} value={data.value}/>
            }
        </ScenarioTableCellInputContainer>
    )
}));

export default ScenarioTableCellInput
