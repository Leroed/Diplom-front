import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {
    ScenarioTableCellHeaderType
} from "../../../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";
import {RootStore} from "../../../../../../../../../stores/RootStore/RootStore";

const ScenarioTableCellHeaderContainer = styled.div`
  width: 100%;
  display: flex;
`

const ScenarioTableCellHeaderValue = styled.div`
  font-size: 12rem;
  font-weight: bold;
  word-break: break-word;
  -webkit-line-clamp: 1;
  max-height: 15rem;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  margin: auto;
`

interface ScenarioTableCellHeaderProps {
    data: ScenarioTableCellHeaderType,
    rootStore?: RootStore
}

export const ScenarioTableCellHeader: React.FC<ScenarioTableCellHeaderProps> = inject('rootStore')(observer((props: ScenarioTableCellHeaderProps) => {
    const {data} = props;
    return (
        <ScenarioTableCellHeaderContainer>
            <ScenarioTableCellHeaderValue>
                {data.value}
            </ScenarioTableCellHeaderValue>
        </ScenarioTableCellHeaderContainer>
    )
}));

export default ScenarioTableCellHeader
