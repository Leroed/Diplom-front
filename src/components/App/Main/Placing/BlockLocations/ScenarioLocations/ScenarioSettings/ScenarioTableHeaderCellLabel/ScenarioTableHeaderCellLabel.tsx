import React, {ReactNode} from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../../../stores/RootStore/RootStore";

const ScenarioTableHeaderCellLabelContainer = styled.div`
  width: 100%;
  display: flex;
`

const ScenarioTableHeaderCellLabelValue = styled.span`
  font-size: 12rem;
  font-weight: bold;
  word-break: break-word;
  -webkit-line-clamp: 1;
  max-height: 15rem;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  margin: auto;
`

interface ScenarioTableHeaderCellLabelProps {
    children?: ReactNode,
    rootStore?: RootStore
}

export const ScenarioTableHeaderCellLabel: React.FC<ScenarioTableHeaderCellLabelProps> = inject('rootStore')(observer((props: ScenarioTableHeaderCellLabelProps) => {

    return (
        <ScenarioTableHeaderCellLabelContainer>
            <ScenarioTableHeaderCellLabelValue>
                {props.children}
            </ScenarioTableHeaderCellLabelValue>
        </ScenarioTableHeaderCellLabelContainer>
    )
}));

export default ScenarioTableHeaderCellLabel
