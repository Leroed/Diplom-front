import React from "react";
import {
    ScenarioTableCellBlockType,
    ScenarioTableCellHeaderType,
    ScenarioTableCellInputType,
    ScenarioTableCellLabelType
} from "../../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";

export type ScenarioTableCellComponent =
    React.FC<{ data: ScenarioTableCellInputType }>
    | React.FC<{ data: ScenarioTableCellLabelType }>
    | React.FC<{ data: ScenarioTableCellHeaderType }>
    | React.FC<{ data: ScenarioTableCellBlockType }>