import React, {useState} from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {
    ScenarioTableCellBlockType,
} from "../../../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";
import {RootStore} from "../../../../../../../../../stores/RootStore/RootStore";

const ScenarioTableCellBlockContainer = styled.div`
  width: 100%;
  min-height: 100%;
  padding: 5rem;
  box-sizing: border-box;
  
`

const ScenarioTableCellBlockRectangle = styled.div<{value: boolean, isHover: boolean}>`
  border-radius: 3rem;
  width: 100%;
  height: 100%;
  @keyframes animationActive {
    0%{
      transform: perspective(500px) rotate3d(0,10,0,0deg) scale(1);
    }
    100%{
      transform: perspective(500px) rotate3d(0,10,0,180deg) scale(1);
    }
  }
  
  @keyframes animationActiveHover {
    0%{
      transform: perspective(500px) rotate3d(0,10,0,180deg) scale(1);
    }
    100%{
      transform: perspective(500px) rotate3d(0,10,0,30deg) scale(1);
    }
  }
  
  @keyframes animationDisactive {
    0%{
      transform: perspective(500px) rotate3d(0,10,0,0deg) scale(1);
    }
    100%{
      transform: perspective(500px) rotate3d(0,10,0,180deg) scale(1);
    }
  }
  
  @keyframes animationDisactiveHover {
    0%{
      transform: perspective(500px) rotate3d(0,10,0,180deg) scale(1);
    }
    100%{
      transform: perspective(500px) rotate3d(0,10,0,30deg) scale(1);
    }
  }

  ${props => {
    if(props.value){
        return `background-color: rgb(80,80,80);
                 border: 1rem solid rgb(70,70,70);
                 animation: 'animationActive' 0.5s ease-in-out;
                 transform: perspective(500px) rotate3d(0,10,0,0deg) scale(1);
                `
    }
    else{
        return `background-color: rgb(230,230,230);
                border: 1rem solid rgb(210,210,210);
                animation: animationDisactive 0.5s ease-in-out;
                transform: perspective(500px) rotate3d(0,10,0,0deg) scale(1);
                `
    }
}};
  
  
${props => {
    if(props.isHover){
        return `box-shadow: 0px 1px 8px 0px rgba(0,0,0,1), 0px 1px 8px 0px rgba(0,0,0,1);`
    }
}};
  transform-style: preserve-3d;
  transition: 0.5s;
  cursor: pointer;
  //transform: rotateY(0deg)  scale(1);
  //transform: perspective(500px) rotate3d(0,0,0,0deg) scale(1);
  transform-style: preserve-3d;


`

// ${props => {
// if(props.isHover){
//     return `transform: perspective(500px) rotate3d(0,30,0,30deg) scale(1);`
// }
// }}

interface ScenarioTableCellBlockProps {
    data: ScenarioTableCellBlockType,
    selectScenariosItemBlock: (id: string) => void,
    rootStore?: RootStore
}

export const ScenarioTableCellBlock: React.FC<ScenarioTableCellBlockProps> = inject('rootStore')(observer((props: ScenarioTableCellBlockProps) => {
    const {data : {value, id}, selectScenariosItemBlock } = props;
    let [isHover, setIsHover] = useState<boolean>(false)


    return (
        <ScenarioTableCellBlockContainer onMouseOut={() => {setIsHover(false)}}
                                         onMouseOver={() => {setIsHover(true)}}>
            <ScenarioTableCellBlockRectangle onClick={() => {selectScenariosItemBlock(id)}}
                                              {...{value, isHover}}>
                &nbsp;
            </ScenarioTableCellBlockRectangle>
        </ScenarioTableCellBlockContainer>
    )
}));

export default ScenarioTableCellBlock
