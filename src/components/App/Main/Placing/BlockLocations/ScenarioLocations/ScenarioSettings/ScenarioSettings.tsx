import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../../stores/RootStore/RootStore";
import ScenarioTableHeaderRow from "./ScenarioTableHeaderRow/ScenarioTableHeaderRow";
import {
    ScenarioTableRowItemType, ScenarioTableRowItemValues,
    ScenarioTableType
} from "../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";
import ScenarioTableRow from "./ScenarioTableRow/ScenarioTableRow";

const ScenarioSettingsContainer = styled.div`

`

interface ScenarioSettingsProps {
    rootStore?: RootStore
}

export const ScenarioSettings: React.FC<ScenarioSettingsProps> = inject('rootStore')(observer((props: ScenarioSettingsProps) => {
    const {currentScenarioTable , selectScenariosItemBlock} = props.rootStore!.placingStore.blockLocationsStore;
    const currentScenarioTableData =  currentScenarioTable || ({id: 0, headers: [], rows: []} as ScenarioTableType);
    const {headers, rows} = currentScenarioTableData;

    return (
        <ScenarioSettingsContainer>
            <ScenarioTableHeaderRow {...{headers}}/>
            {rows.map(({values, id}: {values: ScenarioTableRowItemValues, id: string}) => {
                return <ScenarioTableRow {...{values, selectScenariosItemBlock}} key={id}></ScenarioTableRow>
            })}
        </ScenarioSettingsContainer>
    )
}));

export default ScenarioSettings
