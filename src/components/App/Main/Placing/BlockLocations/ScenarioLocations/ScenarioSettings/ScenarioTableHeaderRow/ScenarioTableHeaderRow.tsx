import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../../../../../../../stores/RootStore/RootStore";
import {
    ScenarioTableHeaderItemType,
    ScenarioTableHeaderType
} from "../../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";
import ScenarioTableCell from "../ScenarioTableCell/ScenarioTableCell";
import ScenarioTableHeaderCellLabel from "../ScenarioTableHeaderCellLabel/ScenarioTableHeaderCellLabel";

const ScenarioTableHeaderRowContainer = styled.div`
  display: flex;
  flex-direction: row;
  min-height: 35rem;
  box-sizing: border-box;
  flex-wrap: nowrap;
`

interface ScenarioTableHeaderRowProps {
    headers: ScenarioTableHeaderType,
    rootStore?: RootStore
}

export const ScenarioTableHeaderRow: React.FC<ScenarioTableHeaderRowProps> = inject('rootStore')(observer((props: ScenarioTableHeaderRowProps) => {
    const {headers} = props;
    return (
        <ScenarioTableHeaderRowContainer>
            {
                headers.map((item: ScenarioTableHeaderItemType) => {
                    return <ScenarioTableCell key={item.id} type={item.id === 'location' ? 'header' : 'label'}>
                        <ScenarioTableHeaderCellLabel>
                            {item.value}
                        </ScenarioTableHeaderCellLabel>
                    </ScenarioTableCell>
                })
            }
        </ScenarioTableHeaderRowContainer>
    )
}));

export default ScenarioTableHeaderRow
