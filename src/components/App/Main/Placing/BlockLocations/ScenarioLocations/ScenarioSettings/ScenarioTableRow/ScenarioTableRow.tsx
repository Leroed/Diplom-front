import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {
    ScenarioTableHeaderItemType,
    ScenarioTableRowItemValue, ScenarioTableRowItemValues
} from "../../../../../../../../stores/RootStore/PlacingStore/BlockLocationsStore/types";
import {RootStore} from "../../../../../../../../stores/RootStore/RootStore";
import ScenarioTableCell from "../ScenarioTableCell/ScenarioTableCell";
import ScenarioTableCellOptional from "../ScenarioTableCellOptional/ScenarioTableCellOptional";

const ScenarioTableRowContainer = styled.div`
  display: flex;
  flex-direction: row;
  min-height: 35rem;
  box-sizing: border-box;
  flex-wrap: nowrap;
`

interface ScenarioTableRowProps {
    values: ScenarioTableRowItemValues,
    selectScenariosItemBlock: (id: string) => void,
    rootStore?: RootStore
}

export const ScenarioTableRow: React.FC<ScenarioTableRowProps> = inject('rootStore')(observer((props: ScenarioTableRowProps) => {
    const {values, selectScenariosItemBlock} = props;

    return (
        <ScenarioTableRowContainer>
            {
                values.map((item: ScenarioTableRowItemValue) => {
                    return <ScenarioTableCell key={item.id} type={item.type}>
                        <ScenarioTableCellOptional {...{selectScenariosItemBlock, data: item}}/>
                    </ScenarioTableCell>
                })
            }
        </ScenarioTableRowContainer>
    )
}));

export default ScenarioTableRow
