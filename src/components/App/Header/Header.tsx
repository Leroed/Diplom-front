import {inject, observer} from "mobx-react";
import React from "react";
import {RootStore} from "../../../stores/RootStore/RootStore";
import styled from "styled-components/macro";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMicroscope} from "@fortawesome/free-solid-svg-icons";
import {NavbarsItem} from "../../../stores/RootStore/NavigationStore/types";


const HeaderContainer = styled.div`
  display: flex;
  width: 100%;
  height: 50rem;
  background-color: rgb(50, 59, 62);
  color: white;
  box-sizing: border-box;
  padding: 0rem 200rem;
`

const HeaderWrapper = styled.header`
  width: 100%;
  display: grid;
  grid-template-columns: 100rem 1fr;
  grid-template-areas: "header-title header-navbars";
`

const HeaderTitle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  grid-area: header-title;
  font-size: 26rem;
  font-width: bold;
  cursor: pointer;
  
  & > *{
    margin-left: 5rem;
  }
`

const HeaderNavbars = styled.div`
  grid-area: header-navbars;
  display: flex;
  justify-content: center;
  align-items: center;
  
  & > *{
    margin-left: 10rem;
  }
`

const HeaderLabel = styled.span`
  
`

const HeaderNavbarsItem = styled.div`
  font-size: 16rem;
  cursor:pointer;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: all 0.5s;
  &:hover{
    transform: translate(0,-5rem);
    ${HeaderLabel}{
    border-bottom: 1rem solid white;
    }
  }
`



interface HeaderProps {
    rootStore?: RootStore
}

export const Header: React.FC<HeaderProps> = inject('rootStore')(observer((props: HeaderProps) => {
    const {navbars, linkToPath} = props.rootStore!.navigationStore;

    return (
        <HeaderContainer>
            <HeaderWrapper>
                <HeaderTitle onClick={() => {linkToPath("/placing/block-locations")}}>
                    <FontAwesomeIcon icon={faMicroscope}/>
                    <HeaderLabel>Placing</HeaderLabel>
                </HeaderTitle>
                <HeaderNavbars>
                    {navbars.map((item: NavbarsItem) => {
                        return (
                            <HeaderNavbarsItem key={item.title} onClick={() => {linkToPath(item.path)}}>
                                <HeaderLabel>
                                    {item.title}
                                </HeaderLabel>
                            </HeaderNavbarsItem>
                        )
                    })}
                </HeaderNavbars>
            </HeaderWrapper>
        </HeaderContainer>
    )
}));

export default Header
