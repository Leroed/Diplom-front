import React from "react";
import {inject, observer} from "mobx-react";
import styled from "styled-components/macro";
import {RootStore} from "../../stores/RootStore/RootStore";
import Header from "./Header/Header";
import Main from "./Main/Main";
import {OpenElementsRef, OpenElementsRefs} from "../../stores/RootStore/RepresentationStore/types";

const AppContainer = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
`

interface AppProps {
    rootStore?: RootStore
}

export const App: React.FC<AppProps> = inject('rootStore')(observer((props: AppProps) => {
    let onClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {

        const openElementsRefs: OpenElementsRefs = props.rootStore!.representationStore.openElementsRefs;

        openElementsRefs.forEach((item: OpenElementsRef) => {
            if (!(e.nativeEvent.composedPath().findIndex((item1) => item1 === item.DOMRef) > -1)) {
                item.onClose();
            }
        });

    };
    return (
        <AppContainer>
            <Header/>
            <Main/>
        </AppContainer>
    )
}));

export default App
