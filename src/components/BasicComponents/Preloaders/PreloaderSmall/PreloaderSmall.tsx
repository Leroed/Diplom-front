import styled from "styled-components";
import {observer} from "mobx-react";
import React from "react";
import "./styles.css"


//
// const LoaderCircle = styled.div`
//     font-size: 10px;
//     margin: 0px auto;
//     text-indent: -9999em;
//     width: 100%;
//     height: 100%;
//     border-radius: 50%;
//     background: #ffffff;
//     background: -moz-linear-gradient(left, #343a40 10%, rgba(255, 255, 255, 0) 42%);
//     background: -webkit-linear-gradient(left, #343a40 10%, rgba(255, 255, 255, 0) 42%);
//     background: -o-linear-gradient(left, #343a40 10%, rgba(255, 255, 255, 0) 42%);
//     background: -ms-linear-gradient(left, #343a40 10%, rgba(255, 255, 255, 0) 42%);
//     background: linear-gradient(to right, #343a40 10%, rgba(255, 255, 255, 0) 42%);
//     position: relative;
//     -webkit-animation: load3 1.4s infinite linear;
//     animation: load3 1.4s infinite linear;
//     -webkit-transform: translateZ(0);
//     -ms-transform: translateZ(0);
//     transform: translateZ(0);
//
//     &:after {
//     background: #0dc5c1;
//     background: white;
//     width: 75%;
//     height: 75%;
//     border-radius: 50%;
//     content: '';
//     margin: auto;
//     position: absolute;
//     top: 0;
//     left: 0;
//     bottom: 0;
//     right: 0;
//     }
//
//     &:before {
//     width: 50%;
//     height: 50%;
//     background: #343a40;
//     border-radius: 100% 0 0 0;
//     position: absolute;
//     top: 0;
//     left: 0;
//     content: '';
//     }
//
//      @-webkit-keyframes load3 {
//     0% {
//       -webkit-transform: rotate(0deg);
//       transform: rotate(0deg);
//     }
//     100% {
//       -webkit-transform: rotate(360deg);
//       transform: rotate(360deg);
//     }
//   }
//   @keyframes load3 {
//     0% {
//       -webkit-transform: rotate(0deg);
//       transform: rotate(0deg);
//     }
//     100% {
//       -webkit-transform: rotate(360deg);
//       transform: rotate(360deg);
//     }
//   }
// `


//
// const LoaderCircle = styled.svg`
//       position: relative;
//       top: 50%;
//       left: 50%;
//       width: 70px;
//       height: 70px;
//       margin-top: -35px;
//       margin-left: -35px;
//       text-align: center;
//       animation: preloader-rotate 2s infinite linear;
//
//       @keyframes preloader-rotate {
//       100% {
//         transform: rotate(360deg);
//       }
//     }
// `

const PreloaderSmallContainer = styled.div`
   
`

interface PreloaderSmallProps {

}

export const PreloaderSmall: React.FC<PreloaderSmallProps> = observer((props) => {

    return (
        <PreloaderSmallContainer>
            <div className='sk-fading-circle'>
                <div className='sk-circle sk-circle-1'></div>
                <div className='sk-circle sk-circle-2'></div>
                <div className='sk-circle sk-circle-3'></div>
                <div className='sk-circle sk-circle-4'></div>
                <div className='sk-circle sk-circle-5'></div>
                <div className='sk-circle sk-circle-6'></div>
                <div className='sk-circle sk-circle-7'></div>
                <div className='sk-circle sk-circle-8'></div>
                <div className='sk-circle sk-circle-9'></div>
                <div className='sk-circle sk-circle-10'></div>
                <div className='sk-circle sk-circle-11'></div>
                <div className='sk-circle sk-circle-12'></div>
            </div>
        </PreloaderSmallContainer>
    )

});

export default PreloaderSmall;