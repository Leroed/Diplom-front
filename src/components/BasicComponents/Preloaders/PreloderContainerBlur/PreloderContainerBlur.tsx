import styled from "styled-components/macro";
import {observer} from "mobx-react";
import React from "react";
import PreloaderSphere from "../PreloaderSphere";


const MainContentContainer = styled.div<{isLoading: boolean}>`
     width: 100%;
     height: 100%;
      ${props => {
        if (props.isLoading) {
            return "filter: blur(5px)"
        }
    }};
`

const PreloaderContainer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
    left: 0px;
  background-color: rgba(100, 100, 100, 0.1);
`

interface PreloderContainerBlurProps {
    isLoading: boolean,
    preloader: React.ReactNode
}

export const PreloderContainerBlur: React.FC<PreloderContainerBlurProps> = observer((props) => {

    const {isLoading, preloader} = props;

    return (
        <>
            <MainContentContainer {...{isLoading}}>{props.children}</MainContentContainer>
            {   isLoading &&
                <PreloaderContainer><PreloaderSphere/></PreloaderContainer>}
        </>
    )

});

export default PreloderContainerBlur;