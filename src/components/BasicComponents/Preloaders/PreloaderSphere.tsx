import styled from "styled-components";
import {observer} from "mobx-react";
import React from "react";
import "./styles.css"


const PreloaderSphereContainer = styled.div`
       display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    
`;

interface PreloaderSphereProps {

}

export const PreloaderSphere: React.FC<PreloaderSphereProps> = observer((props) => {

    return (
        <PreloaderSphereContainer>
            <div>
                <div className='cssload-loader'>
                    <div className='cssload-inner cssload-one'></div>
                    <div className='cssload-inner cssload-two'></div>
                    <div className='cssload-inner cssload-three'></div>
                </div>
            </div>
        </PreloaderSphereContainer>
    )

});

export default PreloaderSphere;