import React, {Component, RefObject} from "react";
import {selectOption} from "../SelectBox/types";
import MultiSelectItem from "../SelectBox/SelectItem/MultiSelectItem";
import SelectTag from "./SelectTag";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import '../SelectBox/style.css'
import {observer} from "mobx-react";
import {closeTag} from "../SelectBox/henchmen";

interface MultiSelectBoxProps {
    listOptions: Array<selectOption>,
    selectList: Array<selectOption>,
    inputRef: RefObject<HTMLInputElement>,

    onElementClickFunc(id: string, options: Array<selectOption>): void,

    onSearchChange(e: React.ChangeEvent<HTMLInputElement>): void
}

interface MultiSelectBoxState {
    isOpened: boolean
}

@observer
export class MultiSelectBox extends Component<MultiSelectBoxProps, MultiSelectBoxState> {
    constructor(props: MultiSelectBoxProps) {
        super(props);
        this.state = {
            isOpened: false
        }
    }

    render() {
        const {onElementClickFunc, selectList, listOptions, inputRef, onSearchChange} = this.props;
        const {isOpened} = this.state;

        return (
            <div className='multi-select'>
                <div className="multi-select_header" onClick={() => {
                    this.setState({isOpened: !isOpened})
                }}>
                    <div className={`multi-select_header-lbl ${selectList ? "checked-select_lbl" : "non-checked-select_lbl"}`}>
                        {selectList.length ?
                                        selectList.map((item: selectOption) => <div className="selection-miniature">{item.displayedValue[0]}</div>)
                                    :
                                        "Выберите значения"
                        }
                    </div>
                    <div className="select-unfold-btn">
                        {isOpened ?
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                 width="100%" height="100%" viewBox="-350 -350 1050.929 1050.929">
                                <g transform='rotate(180 150 150)'>
                                    <path d="M282.082,76.511l-14.274-14.273c-1.902-1.906-4.093-2.856-6.57-2.856c-2.471,0-4.661,0.95-6.563,2.856L142.466,174.441
                                L30.262,62.241c-1.903-1.906-4.093-2.856-6.567-2.856c-2.475,0-4.665,0.95-6.567,2.856L2.856,76.515C0.95,78.417,0,80.607,0,83.082
                                c0,2.473,0.953,4.663,2.856,6.565l133.043,133.046c1.902,1.903,4.093,2.854,6.567,2.854s4.661-0.951,6.562-2.854L282.082,89.647
                                c1.902-1.903,2.847-4.093,2.847-6.565C284.929,80.607,283.984,78.417,282.082,76.511z"/>
                                </g>
                            </svg>
                            :
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                 width="100%" height="100%" viewBox="-350 -350 1050.929 1050.929">
                                <g>
                                    <path d="M282.082,76.511l-14.274-14.273c-1.902-1.906-4.093-2.856-6.57-2.856c-2.471,0-4.661,0.95-6.563,2.856L142.466,174.441
                                L30.262,62.241c-1.903-1.906-4.093-2.856-6.567-2.856c-2.475,0-4.665,0.95-6.567,2.856L2.856,76.515C0.95,78.417,0,80.607,0,83.082
                                c0,2.473,0.953,4.663,2.856,6.565l133.043,133.046c1.902,1.903,4.093,2.854,6.567,2.854s4.661-0.951,6.562-2.854L282.082,89.647
                                c1.902-1.903,2.847-4.093,2.847-6.565C284.929,80.607,283.984,78.417,282.082,76.511z"/>
                                </g>
                            </svg>}
                    </div>
                </div>
                <div className="multi-select_extra">
                    <div className="multi-select_tags">
                        {isOpened && selectList.map((item: selectOption) => <SelectTag value={item.displayedValue}
                                                                                       onClickFunc={() => closeTag(item)}/>)}
                    </div>
                    {
                        isOpened && <>
                          <div className='multi-select_input'>
                            <input className='multi-select_input-field'
                                   ref={inputRef}
                                   type='text'
                                   placeholder={"Поиск"}
                                   onChange={onSearchChange}/>
                            <div className="multi-select_input-icon">
                              <FontAwesomeIcon color={'gray'} icon={faSearch}/>
                            </div>
                          </div>
                          <div className='multi-select_options'>
                              {listOptions.map((item: selectOption) => <MultiSelectItem option={item}
                                                                                        onClickFunc={(id: string) => {
                                                                                       onElementClickFunc(id, listOptions)
                                                                                   }}/>)}
                          </div>
                        </>
                    }
                </div>
            </div>
        )
    }
}

export default MultiSelectBox;