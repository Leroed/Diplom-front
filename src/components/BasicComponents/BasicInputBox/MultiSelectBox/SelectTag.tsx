import React from "react";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

interface SelectTagProps {
    value: string,
    onClickFunc(): void
}

export const SelectTag: React.FC<SelectTagProps> = (props: SelectTagProps) => {
    const {value, onClickFunc} = props;
    return (
        <div className="select-tag">
            <div className="select-tag_value">{value}</div>
            <div className="select-tag_delete-btn" onClick={onClickFunc}>
                <FontAwesomeIcon color={'gray'} icon={faTimes}/>
            </div>
        </div>
    )
};

export default SelectTag;