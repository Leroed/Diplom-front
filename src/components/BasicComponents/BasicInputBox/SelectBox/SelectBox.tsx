import React, {Component, createRef} from "react";
import {selectOption} from './types';
import AdvancedSelect from './AdvancedSelect/AdvancedSelect';
import SimpleSelect from './SimpleSelect/SimpleSelect';
import {inject, observer} from "mobx-react";
import './style.css';
import {RootStore} from "../../../../stores/RootStore/RootStore";

interface SelectBoxProps {
    listOptions: selectOption[],
    isAdvanced: Boolean,
    select: selectOption,

    onSearchChange?(e: React.ChangeEvent<HTMLInputElement>): void,

    onSelectItemClick(id: string, listOptions: selectOption[], select: selectOption): void,

    rootStore?: RootStore
}

interface SelectBoxState {
    isOpened: boolean
}

@inject('rootStore')
@observer
export class SelectBox extends Component<SelectBoxProps, SelectBoxState> {
    constructor(props: SelectBoxProps) {
        super(props);
        this.state = {
            isOpened: false
        };
    }

    componentDidMount(): void {
        this.props.rootStore!.representationStore.addOpenElementRef({
            DOMRef: this.ref.current!, onClose: () => {
                this.setState({isOpened: false});
            }
        });
    }

    ref = createRef<HTMLDivElement>();

    render() {
        const {listOptions, isAdvanced, onSelectItemClick, onSearchChange, select} = this.props;
        const {isOpened} = this.state;

        return (
            <div className='select-box' ref={this.ref}>
                {
                    isAdvanced ? <AdvancedSelect isOpened={isOpened}
                                                 listOptions={listOptions}
                                                 onElementClickFunc={(id: string) => {
                                                     onSelectItemClick(id, listOptions, select);
                                                     this.setState({isOpened: false});
                                                 }}
                                                 select={select}
                                                 inputRef={React.createRef<HTMLInputElement>()}
                                                 onSearchChange={onSearchChange!}
                                                 clickOnUnfoldBtn={() => {
                                                     this.setState({isOpened: !isOpened})
                                                 }}/>
                        : <SimpleSelect isOpened={isOpened}
                                        listOptions={listOptions}
                                        onElementClickFunc={(id: string) => {
                                            onSelectItemClick(id, listOptions, select);
                                            this.setState({isOpened: false});
                                        }}
                                        select={select}
                                        clickOnUnfoldBtn={() => {
                                            this.setState({isOpened: !isOpened})
                                        }}/>
                }
            </div>
        )
    }
}

export default SelectBox;