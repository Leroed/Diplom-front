import React, {Component} from "react";
import { selectOption } from '../types';
import MultiSelectItem from "../SelectItem/MultiSelectItem";
import {observer} from "mobx-react";

interface SimpleSelectProps {
    listOptions: Array<selectOption>,
    isOpened: boolean,
    select: selectOption,
    onElementClickFunc(id: string): void,
    clickOnUnfoldBtn(): void
}

@observer
export class SimpleSelect extends Component<SimpleSelectProps> {
    render() {
        const {listOptions, isOpened, onElementClickFunc, clickOnUnfoldBtn, select} = this.props;
        const currentSelect = listOptions.find((item: selectOption) => item.id === select.id);
        const curSelectValue = currentSelect ? currentSelect.displayedValue : null;

        return (
            <div className = 'simple-select'>
                <div className="simple-select_header" onClick={(event: React.MouseEvent<HTMLDivElement>) => {
                    //event.stopPropagation();
                    clickOnUnfoldBtn()}}>
                    <div className={`simple-select_header-lbl ${curSelectValue ? "checked-select_lbl" : "non-checked-select_lbl"}`}>{curSelectValue ? curSelectValue : "Выберите значение"}</div>
                    <div className="select-unfold-btn">
                        {isOpened ?
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                 width="100%" height="100%" viewBox="-350 -350 1050.929 1050.929">
                                <g transform='rotate(180 150 150)'>
                                    <path d="M282.082,76.511l-14.274-14.273c-1.902-1.906-4.093-2.856-6.57-2.856c-2.471,0-4.661,0.95-6.563,2.856L142.466,174.441
                                L30.262,62.241c-1.903-1.906-4.093-2.856-6.567-2.856c-2.475,0-4.665,0.95-6.567,2.856L2.856,76.515C0.95,78.417,0,80.607,0,83.082
                                c0,2.473,0.953,4.663,2.856,6.565l133.043,133.046c1.902,1.903,4.093,2.854,6.567,2.854s4.661-0.951,6.562-2.854L282.082,89.647
                                c1.902-1.903,2.847-4.093,2.847-6.565C284.929,80.607,283.984,78.417,282.082,76.511z"/>
                                </g>
                            </svg>
                            :
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                 width="100%" height="100%" viewBox="-350 -350 1050.929 1050.929">
                                <g>
                                    <path d="M282.082,76.511l-14.274-14.273c-1.902-1.906-4.093-2.856-6.57-2.856c-2.471,0-4.661,0.95-6.563,2.856L142.466,174.441
                                L30.262,62.241c-1.903-1.906-4.093-2.856-6.567-2.856c-2.475,0-4.665,0.95-6.567,2.856L2.856,76.515C0.95,78.417,0,80.607,0,83.082
                                c0,2.473,0.953,4.663,2.856,6.565l133.043,133.046c1.902,1.903,4.093,2.854,6.567,2.854s4.661-0.951,6.562-2.854L282.082,89.647
                                c1.902-1.903,2.847-4.093,2.847-6.565C284.929,80.607,283.984,78.417,282.082,76.511z"/>
                                </g>
                            </svg>}
                    </div>
                </div>
                {
                    isOpened && <div className = 'simple-select_options'>
                                {listOptions.map((item: selectOption) => <MultiSelectItem option={item} onClickFunc={onElementClickFunc}/>)}
                            </div>
                }
            </div>
        )
    }
}

export default SimpleSelect;