import React from "react";
import {observer} from "mobx-react";
import {selectOption} from "../types";
import {faCheck} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

interface MultiSelectItemProps {
    option: selectOption,
    onClickFunc(id: string): void
}

export const MultiSelectItem = observer((props: MultiSelectItemProps) => {
    const {id, displayedValue, isSelected} = props.option;
    const {onClickFunc} = props;

    return (
        <div className="select-item" onClick={(event: React.MouseEvent<HTMLDivElement>) => {
            //event.stopPropagation();
            onClickFunc(id)}}>
            <div className="select-item_value">{displayedValue}</div>
            {isSelected && <div className="select-item_check-btn"><FontAwesomeIcon color={'green'} icon={faCheck} /></div>}
        </div>
    )
});

export default MultiSelectItem;