import {selectOption} from "./types";

export function getSelectedOptions(listOptions: Array<selectOption>): Array<selectOption> {
    let filteredArr = listOptions.filter((item) => item.isSelected);
    if (filteredArr.length === 0) {
        filteredArr = [{'displayedValue': 'Ничего не выбрано', 'id': 'empty', 'isSelected': true, value: -1}];
    }

    return filteredArr;
}

export function updateSimpleSelectOptionsList(id: string, options: Array<selectOption>, select: selectOption) {
    options.forEach((item: selectOption) => item.isSelected = (item.id === id));
    select.id = id;
}

export function updateMultiSelectOptionsList(id: string, options: Array<selectOption>) {
    options.forEach((item: selectOption) => {
        if (item.id === id) {
            item.isSelected = true;
        }
    });
}

export function closeTag(select: selectOption) {
    select.isSelected = false;
}