import React, {Component, RefObject} from "react";
import {selectOption} from '../types';
import MultiSelectItem from "../SelectItem/MultiSelectItem";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

interface AdvancedSelectProps {
    listOptions: Array<selectOption>,
    isOpened: boolean,
    select: selectOption,
    inputRef: RefObject<HTMLInputElement>,

    onElementClickFunc(id: string): void,

    clickOnUnfoldBtn(): void,

    onSearchChange(e: React.ChangeEvent<HTMLInputElement>): void
}

export class AdvancedSelect extends Component<AdvancedSelectProps> {
    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        const {clickOnUnfoldBtn, onElementClickFunc, onSearchChange, isOpened, listOptions, select} = this.props;
        let {inputRef} = this.props;
        const currentSelect = listOptions.find((item: selectOption) => item.id === select.id);
        const curSelectValue = currentSelect ? currentSelect.displayedValue : null;

        return (
            <div className='advanced-select'>
                <div className="simple-select_header" onClick={(event:React.MouseEvent<HTMLDivElement>) => {
                    //event.stopPropagation();
                    clickOnUnfoldBtn()}}>
                    <div
                        className={`simple-select_header-lbl ${curSelectValue ? "checked-select_lbl" : "non-checked-select_lbl"}`}>{curSelectValue ? curSelectValue : "Выберите значение"}</div>
                    <div className="select-unfold-btn" onClick={clickOnUnfoldBtn}>
                        {isOpened ?
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                 width="100%" height="100%" viewBox="-350 -350 1050.929 1050.929">
                                <g transform='rotate(180 150 150)'>
                                    <path d="M282.082,76.511l-14.274-14.273c-1.902-1.906-4.093-2.856-6.57-2.856c-2.471,0-4.661,0.95-6.563,2.856L142.466,174.441
                                L30.262,62.241c-1.903-1.906-4.093-2.856-6.567-2.856c-2.475,0-4.665,0.95-6.567,2.856L2.856,76.515C0.95,78.417,0,80.607,0,83.082
                                c0,2.473,0.953,4.663,2.856,6.565l133.043,133.046c1.902,1.903,4.093,2.854,6.567,2.854s4.661-0.951,6.562-2.854L282.082,89.647
                                c1.902-1.903,2.847-4.093,2.847-6.565C284.929,80.607,283.984,78.417,282.082,76.511z"/>
                                </g>
                            </svg>
                            :
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                 width="100%" height="100%" viewBox="-350 -350 1050.929 1050.929">
                                <g>
                                    <path d="M282.082,76.511l-14.274-14.273c-1.902-1.906-4.093-2.856-6.57-2.856c-2.471,0-4.661,0.95-6.563,2.856L142.466,174.441
                                L30.262,62.241c-1.903-1.906-4.093-2.856-6.567-2.856c-2.475,0-4.665,0.95-6.567,2.856L2.856,76.515C0.95,78.417,0,80.607,0,83.082
                                c0,2.473,0.953,4.663,2.856,6.565l133.043,133.046c1.902,1.903,4.093,2.854,6.567,2.854s4.661-0.951,6.562-2.854L282.082,89.647
                                c1.902-1.903,2.847-4.093,2.847-6.565C284.929,80.607,283.984,78.417,282.082,76.511z"/>
                                </g>
                            </svg>}
                    </div>
                </div>
                {
                    isOpened && <div className="advanced-select__extra">
                      <div className='advanced-select_input'>
                        <input onFocus={() => clickOnUnfoldBtn()}
                               className='advanced-select_input-field'
                               ref={inputRef}
                               type='text'
                               placeholder={"Поиск"}
                               onChange={onSearchChange}/>
                        <div className="advanced-select_input-icon">
                          <FontAwesomeIcon color={'gray'} icon={faSearch}/>
                        </div>
                      </div>
                      <div className='advanced-select_options'>
                          {listOptions.map((item: selectOption) => <MultiSelectItem option={item}
                                                                                    onClickFunc={onElementClickFunc}/>)}
                      </div>
                    </div>
                }
            </div>
        )
    }
}

export default AdvancedSelect;