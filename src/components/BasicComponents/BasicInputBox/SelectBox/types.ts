export type selectOption = {
    id: string,
    isSelected: boolean,
    displayedValue: string,
    value: any
}

export type selectBoxDataItem = {
    title: string,
    listOptions: selectOption[]
}