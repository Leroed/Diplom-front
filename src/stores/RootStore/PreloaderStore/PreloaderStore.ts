import {RootStore} from "../RootStore";
import {observable, action} from "mobx";

export class PreloaderStore {
    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
    }

    rootStore: RootStore;

    @observable isLoading: boolean = false;

    @action
    isLoadingStatus = (status: boolean) => {
        this.isLoading = status;
    };
}