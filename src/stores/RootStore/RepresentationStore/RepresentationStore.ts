import {RootStore} from "../RootStore";
import {observable, action} from "mobx";
import {OpenElementsRef, OpenElementsRefs} from "./types";

export class RepresentationStore{
    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
    }

    rootStore: RootStore;
    @observable openElementsRefs: OpenElementsRefs = [];

    @action
    addOpenElementRef = (item: OpenElementsRef) => {
        if(!this.openElementsRefs.find((el:OpenElementsRef) => el.DOMRef === item.DOMRef)){
            this.openElementsRefs.push(item);
        }
    };

    @action
    deleteOpenElementRef = (DOMRef: HTMLDivElement | null) => {
        const findIindexElement: number = this.openElementsRefs.findIndex((el:OpenElementsRef) => el.DOMRef === DOMRef);
        if(findIindexElement !== undefined){
            this.openElementsRefs.splice(findIindexElement,1)
        }
    };

}