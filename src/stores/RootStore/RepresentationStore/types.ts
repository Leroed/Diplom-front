export type OpenElementsRef = {
    DOMRef: HTMLDivElement,
    onClose: () => void
};

export type OpenElementsRefs = OpenElementsRef[];