import {RootStore} from "../RootStore";
import {BlockLocationsStore} from "./BlockLocationsStore/BlockLocationsStore";
import {DepartmentsBuildingsStore} from "./DepartmentsBuildingsStore/DepartmentsBuildingsStore";

export class PlacingStore{
    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
    }

    rootStore: RootStore;
    blockLocationsStore: BlockLocationsStore = new BlockLocationsStore(this);
    departmentsBuildingsStore: DepartmentsBuildingsStore = new DepartmentsBuildingsStore(this);

}