import {
    BlocksWithLocationItemType,
    LocationItemType,
    ScenariosItemType,
    ScenariosJSON,
    ScenarioTableHeaderItemType,
    ScenarioTableType,
    ScenarioTableCellTypeVariant,
    BlocksWithLocationType,
    locationsCapacityItemType,
    locationsCapacityType,
    ScenarioTableRowItemValues,
    ScenarioTableRowItemValue,
    ScenarioTableCellInputType,
    ScenarioTableCellHeaderType,
    ScenarioTableCellBlockType,
    ScenarioTableCellLabelType,
    PlacingVariantsLocationsJSON,
    PlacingVariantsLocationsTableType,
    BlocksType,
    LocationsType,
    PlacingVariantsLocationsTableItem,
    PlacingVariantsLocationsTableRow,
    PlacingVariantsLocationsItemRowJSON
} from "./types";
import {selectOption} from "../../../../components/BasicComponents/BasicInputBox/SelectBox/types";
import {values} from "mobx";
import {fixedAndDrop} from "../../../../helpers/formatters";

export const dictBlocks: { [key: number]: string } = {
    1: "РБ",
    2: "CIB",
    3: "БСП",
    4: "УБ",
    5: "ДРПА",
    6: "SBI",
    7: "ДРЭ",
    8: "ДМиК",
    9: "Риски",
    10: "Блок С",
    11: "Блок Т",
    12: "Финансы",
    13: "Рук-во",
    14: "СиР",
    15: "HR",
    16: "GR",
    17: "Аудит",
    18: "Остальное"
};

export const dictLocations: { [key: string]: string } = {
    "Kutuzivsky": "Кутузовский",
    "Skolkovo": "Сколково"
};

const dictTypes: { [key: string]: "input" | "header" } = {"capacity": "input", "location": "header"};

const getTableCell = (
    {headerItem, capacity, itemBlocks, locationItem}:
        { headerItem: ScenarioTableHeaderItemType, capacity: locationsCapacityType, locationItem: LocationItemType, itemBlocks: BlocksWithLocationType }
): ScenarioTableRowItemValue => {
    if (headerItem.id === 'capacity') {
        const findValue: locationsCapacityItemType | undefined = capacity.find(({id}) => id === locationItem.id);
        return ({
            id: `${locationItem.id}-${headerItem.id}`,
            type: "input",
            value: findValue ? findValue.capacity_limit_value : 1
        }) as ScenarioTableCellInputType
    } else if (headerItem.id === 'location') {
        return ({
            id: `${locationItem.id}-${headerItem.id}`,
            type: "header",
            value: locationItem.id !== 999 ? dictLocations[locationItem.name] : locationItem.name
        }) as ScenarioTableCellHeaderType
    } else {
        const findValue = itemBlocks.find(({id}) => id === Number(headerItem.id));
        let cellValue: boolean;
        if (findValue) {
            if ((locationItem.id === 999 && findValue.isModelling) || (locationItem.id === findValue.location)) {
                cellValue = true;
            } else {
                cellValue = false
            }
        } else {
            cellValue = false;
        }
        return ({
            id: `${locationItem.id}-${headerItem.id}`,
            type: "block",
            value: cellValue
        }) as ScenarioTableCellBlockType
    }
};


// export const getTypeCell = (
//     {headerCell, blocks}:
//         { headerCell: ScenarioTableHeaderItemType, blocks?: { [key: number]: string } }
// ): ScenarioTableCellTypeVariant => {
//     const typeCell: "input" | "header" | undefined = dictTypes[headerCell.id];
//     if (typeCell !== undefined) {
//         return typeCell
//     } else {
//         return "block"
//     }
// };

// export const getValueCell = (
//     {cell, type}:
//         { cell: BlocksWithLocationItemType, type: ScenarioTableCellTypeVariant }
// ) => {
//     if(type === 'header' || type === 'label'){
//         return cell.name
//     }
// };

export const parseScenarios = (scenariosData: ScenariosJSON): ScenarioTableType[] => {
    const {blocks, locations, scenarios} = scenariosData;

    const newScenariosTableArr = scenarios.map((item: ScenariosItemType) => {
        const {id, name, blocks: itemBlocks, capacity} = item;
        const blockHeaders: ScenarioTableHeaderItemType[] = blocks.map(({id}: { id: number }) => {
            return ({
                id: String(id),
                value: dictBlocks[id]
            })
        });
        let newScenarioTable: ScenarioTableType = {
            id,
            headers: [{id: 'location', value: " "}, ...blockHeaders, {id: 'capacity', value: "Вместимость"}],
            rows: []
        };

        newScenarioTable.rows = [{
            id: 999,
            name: 'Моделируемые',
            capacity: 0,
            color: ' '
        }, ...locations].map((locationItem: LocationItemType) => {
            let scenarioTableRowItemValues: ScenarioTableRowItemValues = newScenarioTable.headers.map((headerItem: ScenarioTableHeaderItemType) => {
                return getTableCell({headerItem, locationItem, itemBlocks, capacity})
            });
            return {
                id: String(locationItem.id),
                values: scenarioTableRowItemValues
            }
        });

        newScenarioTable.rows = [
            ...newScenarioTable.rows,
            {
                id: "Headcounts",
                values: newScenarioTable.headers.map((headerItem: ScenarioTableHeaderItemType) => {
                    if (headerItem.id === 'location') {
                        return ({
                            id: `headcounts-${headerItem.id}`,
                            type: 'header',
                            value: 'Численность'
                        }) as ScenarioTableCellHeaderType
                    } else if (headerItem.id === "capacity") {
                        return ({
                            id: `headcounts-${headerItem.id}`,
                            type: 'label',
                            value: ' '
                        }) as ScenarioTableCellLabelType
                    } else {
                        const findValue = blocks.find(({id}) => id === Number(headerItem.id));
                        return ({
                            id: `headcounts-${headerItem.id}`,
                            type: 'label',
                            value: findValue ? findValue.headcounts : '0'
                        }) as ScenarioTableCellLabelType
                    }
                })
            }
        ];
        return newScenarioTable

    });

    console.log('newScenariosTableArr', newScenariosTableArr)
    return newScenariosTableArr;
};


export const getSelectOption = (data: selectOption[]): selectOption | null => {
    if (data.length > 0) {
        const findOption: selectOption | undefined = data.find(({isSelected}) => isSelected);
        if (findOption) {
            return findOption
        }
        return null
    }
    return null
};

export const parsePlacingVariantsLocations = (
    {rows, blocks, locations}:
        { rows: PlacingVariantsLocationsItemRowJSON[], blocks: BlocksType, locations: LocationsType }
): PlacingVariantsLocationsTableType => {

    const blockHeaders: PlacingVariantsLocationsTableItem[] = blocks.map(({id}: { id: number }) => {
        return ({
            id: String(id),
            value: dictBlocks[id],
            type: "header"
        })
    });

    let newTable: PlacingVariantsLocationsTableType = {
        headers: [
            {id: "rating", value: "№", type: "header"},
            ...blockHeaders,
            ...locations.map((item: LocationItemType): PlacingVariantsLocationsTableItem => ({
                id: `location-${String(item.id)}`,
                value: dictLocations[item.name],
                type: "header"
            })),
            {id: "quality", value: "Качество", type: "header"},
            {id: "stability", value: "Устойчивость", type: "header"}
        ],
        rows: []
    };



    newTable.rows = rows.map((variantItem: PlacingVariantsLocationsItemRowJSON, index: number): PlacingVariantsLocationsTableRow => {
        return ({
            id: String(variantItem.id),
            values: newTable.headers.map((headerItem: PlacingVariantsLocationsTableItem): PlacingVariantsLocationsTableItem => {
                return getPlacingVariantsLocationsCell({locations, headerItem, index, variantItem})
            })
        })
    });

    console.log("newTable",newTable)
    return newTable
};

export const getPlacingVariantsLocationsCell = (
    {headerItem, variantItem ,index, locations}:
        { headerItem: PlacingVariantsLocationsTableItem,variantItem: PlacingVariantsLocationsItemRowJSON, index: number, locations: LocationsType  }
) : PlacingVariantsLocationsTableItem=> {
    const id = headerItem.id;
    if(headerItem.id === "rating"){
        return {
            id,
            value: String(index + 1),
            type: "header"
        }
    }
    else if(headerItem.id.split("-")[0] === 'location'){
        const findVal = variantItem.locations.find(({id}) => String(id) === headerItem.id.split("-")[1]);
        return {
            id,
            value: `${String(findVal ? fixedAndDrop(findVal.capacity_procent,2) : 1)}%`,
            type: "label"
        }
    }
    else if(headerItem.id === "quality" || headerItem.id === "stability"){
        return {
            id,
            value: `${String(fixedAndDrop(variantItem[headerItem.id], 2))}`,
            type: "label"
        }
    }
    else{
        let findLocationBlock = variantItem.blocks.find(({id}) => String(id) === headerItem.id);
        let color: string;
        if(findLocationBlock){
            const findId = findLocationBlock.location;
            const findLocation = locations.find((locItem: LocationItemType) => (locItem.id === findId));

            color = findLocation ? findLocation.color : 'red';
        }
        else{
            color = 'red'
        }
        //findIdLocation = findIdLocation ? findIdLocation.location :
        return {
            id,
            value: color,
            type: "block"
        }
    }

};