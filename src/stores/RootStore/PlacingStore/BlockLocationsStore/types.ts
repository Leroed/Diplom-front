export type ScenarioTableType = {
    id: number,
    headers: ScenarioTableHeaderType,
    rows: ScenarioTableRowsType
};

export type ScenarioTableCellType = {
    id: string,
    value: string
}

export type ScenarioTableHeaderType = ScenarioTableHeaderItemType[];

export type ScenarioTableHeaderItemType = ScenarioTableCellType;

export type ScenarioTableRowsType = ScenarioTableRowItemType[];

export type ScenarioTableRowItemType = {
    id: string,
    values: ScenarioTableRowItemValues
};

export type ScenarioTableRowItemValues = ScenarioTableRowItemValue[];

export type ScenarioTableRowItemValue =
    ScenarioTableCellBlockType
    | ScenarioTableCellHeaderType
    | ScenarioTableCellLabelType
    | ScenarioTableCellInputType

export type ScenarioTableCellTypeVariant = ("header" | "block" | "input" | "label");

export type ScenarioTableCellBlockType = {
    type: "block",
    id: string,
    value: boolean
}

export type ScenarioTableCellHeaderType = {
    type: "header",
    id: string,
    value: string
}

export type ScenarioTableCellLabelType = {
    type: "label",
    id: string,
    value: string
}

export type ScenarioTableCellInputType = {
    type: "input",
    id: string,
    value: number
}

//     ScenarioTableCellType & {
//     type: "header" | "block" | "input" | "label",
//     isActive?: boolean
// }

export type BlocksType = BlocksItemType[];

export type BlocksItemType = {
    id: number,
    name: string,
    headcounts: number
};

export type LocationsType = LocationItemType[];

export type LocationItemType = {
    id: number,
    name: string,
    capacity: number,
    color: string
}

export type ScenariosType = ScenariosItemType[];

export type ScenariosItemType = {
    id: number,
    name: string,
    blocks: BlocksWithLocationType,
    capacity: locationsCapacityType
};

export type locationsCapacityType = locationsCapacityItemType[];

export type locationsCapacityItemType = {
    id: number,
    name: string,
    capacity: number,
    capacity_limit_value: number
};

export type BlocksWithLocationType = BlocksWithLocationItemType[];

export type BlocksWithLocationItemType = {
    id: number,
    name: string,
    isModelling: boolean,
    location: number
};

export type ScenariosJSON = {
    blocks: BlocksType,
    scenarios: ScenariosType,
    locations: LocationsType
}

export type PlacingVariantsLocationsJSON = {
    blocks: BlocksType,
    rows: PlacingVariantsLocationsItemRowJSON[],
    locations: LocationsType
};

export type PlacingVariantsLocationsItemRowJSON = {
    id: number,
    blocks: PlacingVariantsLocationsBlocks,
    locations: PlacingVariantsLocationsCapacity,
    quality: number,
    stability: number
};

export type PlacingVariantsLocationsBlocks = PlacingVariantsLocationsBlockItem[]

export type PlacingVariantsLocationsBlockItem = {
    id: number,
    name: string,
    location: number
};

export type PlacingVariantsLocationsCapacity = PlacingVariantsLocationsCapacityItem[];

export type PlacingVariantsLocationsCapacityItem = {
    id: number,
    capacity_procent: number
}

export type PlacingVariantsLocationsTableType = {
    headers: PlacingVariantsLocationsTableItem[],
    rows: PlacingVariantsLocationsTableRow[]
};

export type PlacingVariantsLocationsTableItemTypes = 'label' | 'block' | 'header';

export type PlacingVariantsLocationsTableItem= {
    id: string,
    value: string,
    type: PlacingVariantsLocationsTableItemTypes
}

export type PlacingVariantsLocationsTableRow =  {
    id: string,
    values: PlacingVariantsLocationsTableItem[]
}
