import {PlacingStore} from "../PlacingStore";
import {action, computed, observable} from "mobx";
import {
    BlocksType, LocationItemType,
    LocationsType, PlacingVariantsLocationsJSON, PlacingVariantsLocationsTableType,
    ScenariosJSON,
    ScenarioTableRowItemType,
    ScenarioTableRowItemValue,
    ScenarioTableType
} from "./types";
import {selectOption} from "../../../../components/BasicComponents/BasicInputBox/SelectBox/types";
import {blockLocationsApi} from "../../../../api/api";
import {getSelectOption, parsePlacingVariantsLocations, parseScenarios} from "./helpers";

export class BlockLocationsStore {

    constructor(placingStore: PlacingStore) {
        this.placingStore = placingStore;
    }

    placingStore: PlacingStore;

    @observable currentScenarioTable: ScenarioTableType | null = null;
    @observable scenarioTableArr: ScenarioTableType[] = [];
    @observable blocks: BlocksType = [];
    @observable locations: LocationsType = [];
    @observable scenarioOptions: selectOption[] = [];
    @observable isFirstTen: boolean = true;
    @observable placingVariantsLocationsTable: null | PlacingVariantsLocationsTableType = null;

    @action
    restoreData = () => {

    };

    @action
    getScenarios = async () => {
        this.placingStore!.rootStore.preloaderStore.isLoadingStatus(true);
        const scenariosData: ScenariosJSON | null = await blockLocationsApi.getScenarios();
        if (scenariosData) {
            const {blocks, locations, scenarios} = scenariosData;
            this.blocks = blocks;
            this.locations = locations;
            this.scenarioOptions = scenarios.map(({id, name}: { id: number, name: string }, index) => ({
                id: String(id),
                displayedValue: name,
                value: name,
                isSelected: (index === 0 ? true : false)
            }));
            this.scenarioTableArr = parseScenarios(scenariosData);
            this.currentScenarioTable = JSON.parse(JSON.stringify(this.scenarioTableArr[0]));
        } else {
            this.placingStore!.rootStore.preloaderStore.isLoadingStatus(false);
        }
    };

    @action
    selectScenariosItemBlock = (id: string) => {
        const [rowId, columnId] = id.split("-");
        let newValue: boolean = true;
        let linkToModeling: ScenarioTableRowItemValue | undefined;
        if (this.currentScenarioTable) {
            this.currentScenarioTable.rows.forEach((item: ScenarioTableRowItemType) => {
                item.values.forEach((cellItem: ScenarioTableRowItemValue) => {
                    const [rowIdCell, columnIdCell] = cellItem.id.split("-");
                    if (columnIdCell === columnId && cellItem.type === "block") {
                        if (rowId === rowIdCell) {
                            cellItem.value = !cellItem.value;
                            newValue = cellItem.value;
                        } else {
                            cellItem.value = false;
                        }
                        console.log("cellItem",cellItem, rowIdCell)
                        if(rowIdCell === '999'){
                            console.log("go")
                            linkToModeling = cellItem;
                        }
                    }
                })
            })
        };
        if(!newValue && linkToModeling){
            console.log('linkToModeling',linkToModeling)
            linkToModeling.value = true;
        }
    }

    @action
    onSelectScenario = (id: string, listOptions: Array<selectOption>, select: selectOption) => {
        this.scenarioOptions = this.scenarioOptions.map((item) => {
            return {...item, isSelected: (item.id === id)};
        });
    };

    @computed
    get selectScenario(): selectOption | null {
        return getSelectOption(this.scenarioOptions)
    }

    @action
    getPlacingVariantsLocations = async () => {
        this.placingStore!.rootStore.preloaderStore.isLoadingStatus(true);
        const placingVariantsLocationsData: PlacingVariantsLocationsJSON | null = await blockLocationsApi.getPlacingVariantsLocations();
        if (placingVariantsLocationsData) {
            const {blocks, locations, rows} = placingVariantsLocationsData;
            //console.log("placingVariantsLocationsData", placingVariantsLocationsData)
            this.placingVariantsLocationsTable = parsePlacingVariantsLocations({
                rows,
                locations,
                blocks
            });
        } else {
            this.placingStore!.rootStore.preloaderStore.isLoadingStatus(false);
        }
    }


}
