import {PreloaderStore} from "./PreloaderStore/PreloaderStore";
import {PlacingStore} from "./PlacingStore/PlacingStore";
import {NavigationStore} from "./NavigationStore/NavigationStore";
import {RepresentationStore} from "./RepresentationStore/RepresentationStore";

export class RootStore {
    preloaderStore: PreloaderStore = new PreloaderStore(this);
    placingStore: PlacingStore = new PlacingStore(this);
    navigationStore: NavigationStore = new NavigationStore(this);
    representationStore: RepresentationStore = new RepresentationStore(this);
}