export type Navbars = NavbarsItem[];

export type NavbarsItem = {
    title: string,
    path: string
}