import {RootStore} from "../RootStore";
import {action, observable} from "mobx";
import {createBrowserHistory} from 'history'
import {Navbars} from "./types";

export class NavigationStore {
    constructor(rootStore: RootStore) {
        this.rootStore = rootStore;
    }

    rootStore: RootStore;
    @observable history = createBrowserHistory();
    @observable navbars: Navbars = [
        {title: "Размещение по локациям", path: "/placing/block-locations"},
        {title: "Размещение по этажам", path: "/placing/departments-buildings"}
    ];

    @action
    linkToPath = (path: string): void => {
        this.history.push(path);
    }

}